FROM buildpack-deps:stretch-scm
MAINTAINER Konstantin Narkhov <konstantin@narkhov.pro>

ENV PATH            /usr/local/apache2/bin:/usr/local/bin:/usr/local/pgsql/bin:$PATH
ENV LD_LIBRARY_PATH /usr/local/pgsql/lib
ENV HTTPD_VER       2.4.43
ENV PGRES_VER       9.6.18

RUN groupadd apache && useradd -m -g apache apache; \
	apt-get update && apt-get install -y --no-install-recommends \
		g++ \
		gcc \
		libc6-dev \
		make \
		pkg-config \
		sudo \
		cmake \
		bison \
		flex \
		mc \
		socat \
		openssl \
		libencode-perl \
		xz-utils \
		autoconf \
		libltdl-dev \
		libssl-dev \
		build-essential \
		libpcre++-dev \
		libexpat1-dev \
		libreadline-dev \
		zlib1g-dev \
#		postgresql \
#		postgresql-contrib \
		libdbd-pg-perl \
		libcgi-pm-perl \
		libjson-validator-perl \
		libjson-perl \
		libjson-xs-perl \
		libjson-any-perl \
		libtest-json-perl \
		libwww-perl \
		libpq-dev \
		libfile-slurp-perl \
		swig \
	&& rm -rf /var/lib/apt/lists/*; \
	\
# intall perl dependencies
	cd $HOME; \
	mkdir perl; \
	wget https://cpan.metacpan.org/authors/id/P/PE/PEVANS/Scalar-List-Utils-1.54.tar.gz; \
	tar -xzf Scalar-List-Utils-1.54.tar.gz; \
	cd Scalar-List-Utils-1.54; \
	perl Makefile.PL; \
	make; \
	make install; \
	cd $HOME/perl; \
	wget https://cpan.metacpan.org/authors/id/S/SR/SRI/Mojolicious-8.33.tar.gz; \
	tar -xzf Mojolicious-8.33.tar.gz; \
	cd Mojolicious-8.33; \
	perl Makefile.PL; \
	make; \
	make install; \
	cd $HOME/perl; \
	wget https://cpan.metacpan.org/authors/id/J/JH/JHTHORSEN/JSON-Validator-3.23.tar.gz; \
	tar -xzf JSON-Validator-3.23.tar.gz; \
	cd JSON-Validator-3.23; \
	perl Makefile.PL; \
	make; \
	make install; \
	\
# install postgres
	useradd -m -p $(openssl passwd -1 ospasswd) postgres; \
	cd /; \
	mkdir -p $HOME/psql; \
	curl -fsSL https://ftp.postgresql.org/pub/source/v$PGRES_VER/postgresql-$PGRES_VER.tar.gz -o psql.tar.gz; \
	tar xzf psql.tar.gz -C $HOME/psql; \
	rm -f psql.tar.gz; \
	cd $HOME/psql/postgresql-$PGRES_VER; \
	./configure; \
	make world; \
	make install-world; \
	/sbin/ldconfig /usr/local/pgsql/lib; \
	chown -R postgres:postgres /usr/local/pgsql; \
	cd /usr/local/pgsql; \
	sudo -u postgres /usr/local/pgsql/bin/initdb --pgdata=/usr/local/pgsql/data --encoding=UTF8 --no-locale; \
	sudo -u postgres sed -Ei 's/([[:digit:]]+[[:space:]]+)trust/\1md5/g' /usr/local/pgsql/data/pg_hba.conf; \
	\
# install httpd
	cd /; \
	mkdir -p $HOME/apache; \
	curl -fsSL https://archive.apache.org/dist/httpd/httpd-$HTTPD_VER.tar.gz -o httpd.tar.gz; \
	curl -fsSL http://apache-mirror.rbc.ru/pub/apache/apr/apr-1.7.0.tar.gz -o apr.tar.gz; \
	curl -fsSL http://apache-mirror.rbc.ru/pub/apache/apr/apr-util-1.6.1.tar.gz -o apr-util.tar.gz; \
	tar xzf httpd.tar.gz -C $HOME/apache; \
	tar xzf apr.tar.gz -C $HOME/apache/httpd-$HTTPD_VER/srclib; \
	tar xzf apr-util.tar.gz -C $HOME/apache/httpd-$HTTPD_VER/srclib; \
	mv $HOME/apache/httpd-$HTTPD_VER/srclib/apr-1.7.0 $HOME/apache/httpd-$HTTPD_VER/srclib/apr; \
	mv $HOME/apache/httpd-$HTTPD_VER/srclib/apr-util-1.6.1 $HOME/apache/httpd-$HTTPD_VER/srclib/apr-util; \
	rm -f httpd.tar.gz apr.tar.gz apr-util.tar.gz; \
	cd $HOME/apache/httpd-$HTTPD_VER; \
	./configure --prefix=/usr/local/apache2 --with-included-apr --enable-mods-shared="all" --enable-suexec --with-suexec-docroot=/home/apache --with-suexec-caller=daemon; \
	make; \
	make install; \
# configure httpd
	mkdir -p /home/apache/apache_root/logs /home/apache/git/pheix-scada /home/apache/apache_root/cgi-bin; \
	git clone https://gitlab.com/pheix-scada/io-sw.git /home/apache/git/pheix-scada/io-sw; \
	git clone https://gitlab.com/pheix-scada/io-database.git /home/apache/git/pheix-scada/io-database; \
	ln -s /home/apache/git/pheix-scada/io-sw/perl /home/apache/apache_root/www; \
	ln -s /home/apache/git/pheix-scada/io-database/swig /home/apache/apache_root/www/IO; \
	sed -i 's/Listen 80/Listen 8888/g' /usr/local/apache2/conf/httpd.conf; \
	sed -i 's/#LoadModule suexec_module modules\/mod_suexec.so/LoadModule suexec_module modules\/mod_suexec\.so/g' /usr/local/apache2/conf/httpd.conf; \
	sed -i 's/#LoadModule rewrite_module modules\/mod_rewrite.so/LoadModule rewrite_module modules\/mod_rewrite.so/g' /usr/local/apache2/conf/httpd.conf; \
	sed -i 's/#LoadModule cgid_module modules\/mod_cgid.so/LoadModule cgid_module modules\/mod_cgid.so/g' /usr/local/apache2/conf/httpd.conf; \
	echo 'ServerName scada.docker' >> /usr/local/apache2/conf/httpd.conf; \
	echo '<VirtualHost *:8888>' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'ServerAdmin admin@docker' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'DocumentRoot "/home/apache/apache_root/www"' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'ScriptAlias /cgi-bin/ "/home/apache/apache_root/cgi-bin/"' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'ServerName scada.docker' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'SuexecUserGroup apache apache' >> /usr/local/apache2/conf/httpd.conf; \
	echo '<Directory "/home/apache/apache_root/www">' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'Require all granted' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'Options ExecCGI SymLinksIfOwnerMatch Indexes Includes MultiViews' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'AllowOverride All' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'Order allow,deny' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'Allow from all' >> /usr/local/apache2/conf/httpd.conf; \
	echo '</Directory>' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'ErrorLog "/home/apache/apache_root/logs/error.log"' >> /usr/local/apache2/conf/httpd.conf; \
	echo 'CustomLog "/home/apache/apache_root/logs/access.log" common' >> /usr/local/apache2/conf/httpd.conf; \
	echo '</VirtualHost>' >> /usr/local/apache2/conf/httpd.conf; \
	echo '127.0.0.1 webtech.host scada.docker' >> /etc/hosts; \
	chown -R apache.apache /home/apache/*; \
	chmod g-w /home/apache/git/pheix-scada/io-sw/perl /home/apache/git/pheix-scada/io-sw/perl/io.pl; \
	cp /home/apache/git/pheix-scada/io-sw/helpers/init.sh /; \
	cp /home/apache/git/pheix-scada/io-sw/helpers/start.sh /; \
	cp /home/apache/git/pheix-scada/io-sw/helpers/status.sh /; \
	chmod 755 /*.sh; \
	chmod 755 /home/apache/git/pheix-scada/io-sw/perl/*.pl; \
	cd /home/apache/git/pheix-scada/io-database; \
	make lib; \
	cd /home/apache/git/pheix-scada/io-database/swig; \
	swig -perl5 iodb.i && ./compile.sh && ./compile-swig.sh;

CMD ["/bin/bash"]
