# io-docker-scada

Docker container for IO server.

## Build

```bash
sudo docker build -t io-server .
```

## Run

```bash
sudo docker run --net=host -it io-server
```

## Use

After `Run` stage you will enter into containet console. Next step is to execute:

```bash
./init.sh
```

On host check PostgreSQL connection via

```bash
psql --host=127.0.0.1 -U postgres -W
```

Note: use `ospasswd` password for user [+&nbsp;&nbsp;postgres&nbsp;&nbsp;+].

Check HTTPd connection via firefox on [http://127.0.0.1:8888](http://127.0.0.1:8888).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).