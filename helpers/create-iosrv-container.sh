#!/bin/bash

docker stop iosrv && docker rm iosrv

docker run -v /home/osuser/iosrv/mount:/mnt -d --net=host --name=iosrv iosrv /bin/bash -c "/start.sh && tail -f /dev/null"

sleep 10 && docker ps -a
